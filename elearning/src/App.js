import React from 'react';
import './App.css';
import HomeScreen from './Screens/Home';
import CourseDetailScreen from './Screens/Detail';
import SignupScreen from './Screens/Signup';
import Header from './Layouts/Header'
import { BrowserRouter, Route, Switch } from 'react-router-dom'
import SigninScreen from './Screens/Signin';

function App() {
  return (
    <BrowserRouter>
      <Header />
      <Switch>
        <Route path="/detail/:courseId" component={CourseDetailScreen} />
        <Route path="/signup" component={SignupScreen} />
        <Route path="/signin" component={SigninScreen} />
        <Route path="/" component={HomeScreen} />
      </Switch>
      {/* <HomeScreen/> */}
      {/* <CourseDetailScreen /> */}
      {/* <SignupScreen/> */}
    </BrowserRouter>
  );
}

export default App;
