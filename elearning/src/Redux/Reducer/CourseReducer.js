const initialState = {
    courses: [],
    courseDetail: null
}

const CourseReducer = (state = initialState, { type, payload }) => {
    switch (type) {
        case 'FETCH_COURSES':
            state.courses = payload;
            return { ...state }
        case 'FETCH_COURSE_DETAIL':
            state.courseDetail = payload;
            return { ...state }
        default:
            return { ...state }
    }
}

export default CourseReducer;