import { FETCH_CREDENTIALS } from "../Action/type";

const initialState = {
    credentials: null
}

const UserReducer = (state = initialState, {type,payload}) => {
    switch(type){
        case FETCH_CREDENTIALS:
            state.credentials = payload
            return {...state}
        default:
            return {...state}
    }
}

export default UserReducer;

