import { courseService } from "../../Service";
import { createAction } from ".";
import { FETCH_COURSES, FETCH_COURSE_DETAIL } from "./type";


// async action
export const fetchCourses = () => {
    return (dispatch) => {
        // axios return promise ES6
        courseService.fetchCourses()
            // promise
            .then((res) => {
                dispatch(createAction(FETCH_COURSES, res.data))
            }).catch((err) => {
                console.log(err)
            });
    }
}

export const fetchDetailCourse = (courseId) => {
    return (dispatch) => {
        courseService.fetchCourseDetail(courseId).then((res) => {
            dispatch(createAction(FETCH_COURSE_DETAIL, res.data))
        }).catch((err) => {
            console.log(err)
        });
    }
}