import { userService } from "../../Service"
import { createAction } from "."
import { FETCH_CREDENTIALS } from "./type"

export const signIn = (data) => {
    return (dispatch) => {
        userService.signIn(data)
            .then(res => {
                dispatch(createAction(FETCH_CREDENTIALS, res.data))
                localStorage.setItem('credentials',JSON.stringify(res.data))
            })
            .catch(err => {
                console.log(err)
            })
    }
}