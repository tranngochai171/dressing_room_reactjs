import React, { Component } from 'react'
import {Link} from 'react-router-dom'
export default class CourseItems extends Component {
    render() {
        const {item} = this.props;
        return (
            <div className="card p-2">
                <img src={item.hinhAnh} style={{width:'100%',height:'200px'}}/>
                <p className="lead font-weight-bold">{item.tenKhoaHoc}</p>
                <p className="lead">Instructor: Ngoc Hai</p>
                <p className="lead">Rating: 5.0</p>
                <Link to={`/detail/${item.maKhoaHoc}`} className="btn btn-success">Go to Detail</Link>
            </div>
        )
    }
}
