import Axios from 'axios';
class CourseService{
    fetchCourses(){
        return Axios({
            method: 'GET',
            url: 'http://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01'
        })
    }
    fetchCourseDetail(maKhoaHoc){
        return Axios({
            method: 'GET',
            url: `http://elearning0706.cybersoft.edu.vn/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${maKhoaHoc}`
        })
    }
}

export default CourseService;