import React, { Component } from 'react'
import { Formik, Form, Field, ErrorMessage } from 'formik'

import { userService } from '../../Service';
import { signupUserSchema } from '../../Service/user';

export default class SignupScreen extends Component {
    _handleSubmit = (value) => {
        userService.signUp(value).then(res => {
            console.log(res)
        }).catch(err => {
            console.log(err)
        })
    }
    render() {
        return (
            <div className="w-50 mx-auto">
                <h1 className="display-4 text-center">Sign Up</h1>
                <Formik
                    initialValues={{
                        taiKhoan: '',
                        matKhau: '',
                        hoTen: '',
                        email: '',
                        soDT: '',
                        maNhom: 'GP01'
                    }}
                    validationSchema={signupUserSchema}
                    onSubmit={this._handleSubmit}
                    render={(formilProps) => (
                        <Form>
                            <div className="form-group">
                                <label>Tai khoan: </label>
                                <Field type="text" className="form-control" name="taiKhoan" onChange={formilProps.handleChange} />
                                <ErrorMessage name="taiKhoan" />
                            </div>
                            <div className="form-group">
                                <label>Mat khau: </label>
                                <Field type="password" className="form-control" name="matKhau" onChange={formilProps.handleChange} />
                                <ErrorMessage name="matKhau" />
                            </div>
                            <div className="form-group">
                                <label>Ho ten: </label>
                                <Field type="text" className="form-control" name="hoTen" onChange={formilProps.handleChange} />
                                <ErrorMessage name="hoTen" />
                            </div>
                            <div className="form-group">
                                <label>Email: </label>
                                <Field type="email" className="form-control" name="email" onChange={formilProps.handleChange} />
                                <ErrorMessage name="email" />
                            </div>
                            <div className="form-group">
                                <label>So dien thoai: </label>
                                <Field type="text" className="form-control" name="soDT" onChange={formilProps.handleChange} />
                                <ErrorMessage name="soDT">
                                    {
                                        (msg) => <div className="alert alert-danger">{msg}</div>
                                    }
                                </ErrorMessage>
                            </div>
                            <div className="form-group">
                                <label>Ma nhom: </label>
                                <Field component="select" className="form-control" name="maNhom" onChange={formilProps.handleChange}>
                                    <option>GP01</option>
                                    <option>GP02</option>
                                    <option>GP03</option>
                                    <option>GP04</option>
                                    <option>GP05</option>
                                    <option>GP06</option>
                                    <option>GP07</option>
                                    <option>GP08</option>
                                    <option>GP09</option>
                                    <option>GP10</option>
                                </Field>
                                <ErrorMessage name="maNhom" />
                            </div>
                            <div className="text-center">
                                <button type="submit" className="btn btn-success">Submit</button>
                            </div>
                        </Form>
                    )} />
            </div>
        )
    }
}
