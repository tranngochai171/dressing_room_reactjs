import React, { Component } from 'react'
import { Form, ErrorMessage, Formik, Field } from 'formik'
import { signinUserSchema } from '../../Service/user'
import {connect} from 'react-redux';
import { signIn } from '../../Redux/Action/user';
class SigninScreen extends Component {
    _handleSubmit = (value) => {
        this.props.dispatch(signIn(value))
    }
    render() {
        return (
            <Formik 
            initialValues={{
                taiKhoan: '',
                matKhau: ''
            }}
            validationSchema={signinUserSchema}
            onSubmit={this._handleSubmit}
            render={({handleChange}) => (
                <Form className="w-50 mx-auto">
                    <h1>SIGN IN</h1>
                    <div className="form-group">
                        <label>Tai Khoan:</label>
                        <Field type="text" name="taiKhoan" className="form-control" onChange={handleChange}/>
                        <ErrorMessage name="taiKhoan" />
                    </div>
                    <div className="form-group">
                        <label>Mat Khau:</label>
                        <Field type="password" name="matKhau" className="form-control" onChange={handleChange}/>
                        <ErrorMessage name="matKhau" />
                    </div>
                    <button type="submit" className="btn btn-success">Dang nhap</button>
                </Form>
            )} />
        )
    }
}

export default connect()(SigninScreen);