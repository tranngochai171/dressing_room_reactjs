import React, { Component } from 'react'
import { connect } from 'react-redux';
import { fetchDetailCourse } from '../../Redux/Action/course';
class CourseDetailScreen extends Component {
    render() {
        const { courseDetail } = this.props
        return (
            <div>
                <img src={courseDetail.hinhAnh} />
                <h3>{courseDetail.tenKhoaHoc}</h3>
            </div>
        )
    }
    componentDidMount() {
        const { courseId } = this.props.match.params
        this.props.dispatch(fetchDetailCourse(courseId))
    }
}

const mapStateToProps = (state) => {
    return {
        courseDetail: state.CourseReducer.courseDetail || {
            maKhoaHoc: '',
            tenKhoaHoc: '',
            hinhAnh: '',
            nguoiTao: {
                taiKhoan: '',
                hoTen: ''
            }
        }
    }
}

export default connect(mapStateToProps, null)(CourseDetailScreen);
