import React, { Component } from 'react'
import CourseItems from '../../Components/CourseItems'
import { connect } from 'react-redux';
import { fetchCourses } from '../../Redux/Action/course';
class HomeScreen extends Component {
    renderCourses = () => {
        const { courses } = this.props
        return courses.map((item, index) => {
            return (
                <div key={index} className="col-3">
                    <CourseItems item={item} />
                </div>
            )
        })
    }
    render() {
        return (
            <div>
                <h1 className="display-4 text-center">Danh sach khoa hoc</h1>
                <div className="container">
                    <div className="row">
                        {this.renderCourses()}
                    </div>
                </div>
            </div>
        )
    }
    componentDidMount() {
        this.props.dispatch(fetchCourses())
    }
}

const mapStateToProps = (state) => {
    return {
        courses: state.CourseReducer.courses
    }
}

export default connect(mapStateToProps, null)(HomeScreen);