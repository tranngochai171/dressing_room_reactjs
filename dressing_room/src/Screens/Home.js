import React, { Component } from 'react'
import Header from '../Layouts/Header/Header'
import Footer from '../Layouts/Footer/Footer'
import CategoryComponent from '../Components/Categories/Category'
import ProductComponents from '../Components/Products/Products'
import ModelComponent from '../Components/Model/Model'

export default class Home extends Component {
    render() {
        return (
            <div>
                <Header />
                <div className="container-fluid">
                    <div className="row">
                        <div className="col-6">
                            <CategoryComponent />
                            <ProductComponents />
                        </div>
                        <div className="col-6">
                            <ModelComponent />
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        )
    }
}
