import {combineReducers} from 'redux';
import ProductReducer from './ProductReducer';
import CategoryReducer from './CategoryReducer';
import ModelReducer from './ModelReducer';

export const RootReducer = combineReducers({
    ProductReducer,
    CategoryReducer,
    ModelReducer
})