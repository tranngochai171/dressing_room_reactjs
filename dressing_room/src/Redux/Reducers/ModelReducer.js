const initialState = {
    topclothes: '',
    botclothes: '',
    shoes: '',
    handbags: '',
    necklaces: '',
    hairstyle: '',
    background: ''
}

const ModelReducer = (state = initialState, {type,payload}) => {
    switch(type){
        case 'TRY_IT_OUT':
            return {...state,[payload.type]:payload.img}
        default:
            return {...state}
    }
    
}

export default ModelReducer;