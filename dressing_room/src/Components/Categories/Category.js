import React, { Component } from 'react'
import { connect } from 'react-redux';
class CategoryComponent extends Component {
    renderCategories = () => {
        const { categories,chooseCategory } = this.props;
        return categories.map((item, index) => {
            return <button onClick={()=>chooseCategory(item.type)} key={index} className="btn btn-success">{item.showName}</button>
        })
    }
    render() {
        return (
            <div className="btn-group">
                {this.renderCategories()}
            </div>
        )
    }
}
const mapStateToProps = (state) => {
    return {
        categories: state.CategoryReducer.categories
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        chooseCategory : (type) => dispatch({
            type: 'CHOOSE_CATEGORY',
            payload: type
        })
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CategoryComponent);