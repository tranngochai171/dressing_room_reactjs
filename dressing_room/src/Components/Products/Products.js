import React, { Component } from 'react'
import ProductItemComponent from '../ProductItem/ProductItem'
import { connect } from 'react-redux';
class ProductComponents extends Component {
    renderProduct = () => {
        const {products,chosenCategory} = this.props;
        return products.filter(item => chosenCategory === item.type).map((item,index)=>{
            return (<ProductItemComponent key={index} item={item}/>)
        })
    }
    render() {
        return (
            <div className="row">
                {this.renderProduct()}
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        products: state.ProductReducer,
        chosenCategory: state.CategoryReducer.chosenCategory
    }
}

export default connect(mapStateToProps, null)(ProductComponents);