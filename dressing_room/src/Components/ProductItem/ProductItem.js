import React, { Component } from 'react'
import {connect} from 'react-redux'
class ProductItemComponent extends Component {
    render() {
        let { item,tryItOut } = this.props;
        return (
            <div className="card p-2 my-2">
                <img src={item.imgSrc_jpg} />
                <p className="text-center">{item.name}</p>
                <button onClick={()=>tryItOut(item)} className="btn btn-success">Try</button>
            </div>
        )
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        tryItOut :(item) => dispatch({
            type: 'TRY_IT_OUT',
            payload: {
                type: item.type,
                img: item.imgSrc_png
            }
        })
    }
}

export default connect(null,mapDispatchToProps)(ProductItemComponent);