import React, { Component } from 'react'
import classes from './style.module.css'
import {connect} from 'react-redux'
class ModelComponent extends Component {
    constructor(props) {
        super(props)

        this.state = {
            contain: 'images/background/background_998.jpg',
            body: 'images/allbody/bodynew.png',
            bikinitop: 'images/allbody/bikini_branew.png',
            bikinibottom: 'images/allbody/bikini_pantsnew.png',
            model: 'images/model/1000new.png',
            feetleft: 'images/allbody/feet_high_leftnew.png',
            feetright: 'images/allbody/feet_high_rightnew.png'
        }
    }

    render() {
        const {model} = this.props;
        return (
            <div className={classes.contain} style={{ background: `url(${this.state.contain})` }}>
                <div className={classes.body} style={{ background: `url(${this.state.body})` }}>
                </div>
                <div className={classes.model} style={{ background: `url(${this.state.model})` }}>
                </div>
                <div className={classes.bikinitop} style={{ background: `url(${this.state.bikinitop})` }}>
                </div>
                <div className={classes.bikinibottom} style={{ background: `url(${this.state.bikinibottom})` }}>
                </div>
                <div className={classes.feetleft} style={{ background: `url(${this.state.feetleft})` }}>
                </div>
                <div className={classes.feetright} style={{ background: `url(${this.state.feetright})` }}>
                </div>

                <div className={classes.bikinitop} style={{ backgroundImage: `url(${model.topclothes})`,backgroundSize: 'cover' }}>
                </div>
                <div className={classes.bikinibottom} style={{ backgroundImage: `url(${model.botclothes})`,backgroundSize: 'cover'  }}>
                </div>
                <div className={classes.feetStyle} style={{ backgroundImage: `url(${model.shoes})`,backgroundSize: 'cover'  }}>
                </div>
                <div className={classes.handbagStyle} style={{ backgroundImage: `url(${model.handbags})`,backgroundSize: 'cover'  }}>
                </div>
                <div className={classes.hairStyle} style={{ backgroundImage: `url(${model.hairstyle})` ,backgroundSize: 'cover' }}>
                </div>
                <div className={classes.feetright} style={{ backgroundImage: `url(${model.background})` ,backgroundSize: 'cover' }}>
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        model: state.ModelReducer
    }
}

export default connect(mapStateToProps,null)(ModelComponent);